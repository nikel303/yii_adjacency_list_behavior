<?php

namespace YiiAdjacencyList;

class Behavior extends \CActiveRecordBehavior
{

    public $parentAttribute = 'pid';
    public $levelAttribute = 'level';
    public $pathAttribute = 'path';
    public $scopeAttribute = 'scope_id';
    public $multyRoot = false;
    //
    public $buildUrl = false;
    public $urlAttribute = 'url';
    public $urlPartAttribute = 'alias';
    public $urlPartDelemiter = '/';
    //
    public $calculateChildrens = false;
    public $childrensAttribute = 'childrens';
    //
    public $useCache = true;

    protected function _applyMultyRootCriteria(\CDbCriteria & $criteria)
    {

        if ($this->multyRoot)
            $criteria->addColumnCondition([$this->scopeAttribute => $this->getOwner()->{$this->scopeAttribute}]);
    }

    public function getPathIds()
    {

        $oOwner = $this->getOwner();
        $sPath = $oOwner->{$this->pathAttribute};

        return !empty($sPath) ? explode('/', $sPath) : ((int)(($id = $oOwner->getPrimaryKey())) > 0 ? [$id] : []);
    }

    public function getParentIds()
    {
        return array_diff($this->getPathIds(), [$this->getOwner()->getPrimaryKey()]);
    }

    public function getChildrensFindCondition($depth = 0, $criteria = null)
    {

        $oOwner = $this->getOwner();

        $_oCriteria = new \CDbCriteria;

        $_oCriteria->addCondition('t.`path` LIKE :path');
        $_oCriteria->params[':path'] = $oOwner->path . '/%';

        $depth = (int)$depth;
        switch (true) {

            case ($depth == 1): {

                $_oCriteria->addColumnCondition(['level' => (int)$oOwner->level + 1]);
                break;
            }

            case ($depth > 1): {

                $_oCriteria->addBetweenCondition('level', (int)$oOwner->level, (int)$oOwner->level + $depth);
                break;
            }
        }

        if (null !== $criteria) {
            $_oCriteria->mergeWith($criteria);
        }

        $this->_applyMultyRootCriteria($_oCriteria);

        return $_oCriteria;
    }

    public function getChildrens($depth = 0, $criteria = null)
    {

        return $this->getOwner()->findAll($this->getChildrensFindCondition($depth, $criteria));
    }

    public function getChildrenIdsCalculated()
    {

        if (false === $this->calculateChildrens)
            throw new CException('Подсчет дочерних элементов для данной модели не активен');

        $owner = $this->getOwner();

        static $cache = [];
        if (!isset($cache[(int)$owner->id])) {

            $cache[(int)$owner->id] = [];

            if (!empty($owner->{$this->childrensAttribute})) {

                $cache[(int)$owner->id] = explode(',', $owner->{$this->childrensAttribute});

                $cache[(int)$owner->id] = array_map(function ($v) {
                    return (int)$v;
                }, $cache[(int)$owner->id]);

                $cache[(int)$owner->id] = array_filter($cache[(int)$owner->id], function ($v) {
                    return $v > 0;
                });
            }
        }

        return $cache[(int)$owner->id];
    }

    public function getChildrensId($depth = 0, $criteria = null)
    {

        $oOwner = $this->getOwner();

        $_oCriteria = new \CDbCriteria;

        $_oCriteria->select = 't.`id`';
        $_oCriteria->addCondition('t.`path` LIKE :path');
        $_oCriteria->params[':path'] = $oOwner->path . '/%';

        $depth = (int)$depth;
        switch (true) {

            case ($depth == 1): {

                $_oCriteria->addColumnCondition(['level' => (int)$oOwner->level + 1]);
                break;
            }

            case ($depth > 1): {

                $_oCriteria->addBetweenCondition('level', (int)$oOwner->level, (int)$oOwner->level + $depth);
                break;
            }
        }

        if (null !== $criteria) {
            $_oCriteria->mergeWith($criteria);
        }

        $this->_applyMultyRootCriteria($_oCriteria);

        $command = $oOwner->getDbConnection()->getCommandBuilder()->createFindCommand($oOwner->tableName(), $_oCriteria);

        return $command->queryColumn();
    }

    public function getParent($criteria = null)
    {

        $oResult = null;
        $oOwner = $this->getOwner();

        if ((int)($id = $oOwner->{$this->parentAttribute}) > 0) {

            $_oCriteria = new \CDbCriteria;
            $_oCriteria->mergeWith($oOwner->getDbCriteria());

            if (null !== $criteria)
                $_oCriteria->mergeWith($criteria);

            $this->_applyMultyRootCriteria($_oCriteria);

            $oResult = $oOwner->findByPk($id, $_oCriteria);
        }

        return $oResult;
    }

    public function getParents($criteria = null)
    {

        $oResult = [];

        $aIds = $this->getParentIds();
        if (sizeof($aIds) > 0) {

            $oOwner = $this->getOwner();

            if ($this->useCache)
                static $_cache = [];

            $sCacheId = md5(serialize([get_class($oOwner), $aIds, $criteria]));
            if (!$this->useCache || !isset($_cache[$sCacheId])) {

                $pkCol = $oOwner->getMetaData()->tableSchema->primaryKey;

                $_oCriteria = new \CDbCriteria;

                $_oCriteria->mergeWith($oOwner->getDbCriteria());

                if (null !== $criteria)
                    $_oCriteria->mergeWith($criteria);

                $this->_applyMultyRootCriteria($_oCriteria);

                $_oCriteria->addInCondition($pkCol, $aIds);

                $oResult = $oOwner->findAll($_oCriteria);

                if ($this->useCache)
                    $_cache[$sCacheId] = $oResult;
            } else {

                $oResult = $_cache[$sCacheId];
            }
        }

        return $oResult;
    }

    public function getPathAsString($titleAttribute = 'title', $sSep = ' → ')
    {

        $oOwner = $this->getOwner();

        $sPath = [];

        foreach ($this->getParents() as $itm) {
            $sPath[] = $itm->{$titleAttribute};
        }

        $sPath[] = $oOwner->{$titleAttribute};

        return implode($sSep, $sPath);
    }

    public function getPathArray()
    {

        $path = $this->getParents();
        $path[] = $this->getOwner();

        return $path;
    }

    public function getBreadcrumbs($titleAttribute = 'title', $urlAttribute = 'url', $lastActive = false)
    {

        $oOwner = $this->getOwner();

        $sPath = [];

        foreach ($this->getParents() as $itm) {
            $sPath[$itm->{$titleAttribute}] = $itm->{$urlAttribute};
        }

        if ($lastActive) {
            $sPath[$oOwner->{$titleAttribute}] = $oOwner->{$urlAttribute};
        } else {
            $sPath[] = $oOwner->{$titleAttribute};
        }

        return $sPath;
    }

    public function getBreadcrumbsExt($titleAttribute = 'title', $urlAttribute = 'url', $lastActive = false)
    {

        $oOwner = $this->getOwner();

        $titleCallable = is_callable($titleAttribute);
        $urlCallable = is_callable($urlAttribute);

        $sPath = [];

        foreach ($this->getParents() as $itm) {

            $title = $titleCallable ? call_user_func($titleAttribute, $itm) : $itm->{$titleAttribute};
            $url = $urlCallable ? call_user_func($urlAttribute, $itm) : $itm->{$urlAttribute};

            $sPath[$title] = $url;
        }

        $title = $titleCallable ? call_user_func($titleAttribute, $oOwner) : $oOwner->{$titleAttribute};
        $url = $urlCallable ? call_user_func($urlAttribute, $oOwner) : $oOwner->{$urlAttribute};

        if ($lastActive) {
            $sPath[$title] = $url;
        } else {
            $sPath[] = $title;
        }

        return $sPath;
    }

    public function calculateLevelPath()
    {

        $oOwner = $this->getOwner();

        $attributeToSave = [];

        $aPath = [];
        $aUrl = [];

        if ((int)$oOwner->{$this->parentAttribute} > 0 && null !== ($oParent = $this->getParent())) {

            $aPath = $oParent->getPathIds();

            if ($this->buildUrl)
                $aUrl[] = $oParent->{$this->urlAttribute};
        }

        $aPath[] = $oOwner->getPrimaryKey();
        $sPath = implode('/', $aPath);

        if ($this->buildUrl) {

            $aUrl[] = $oOwner->{$this->urlPartAttribute};
            $sUrl = implode($this->urlPartDelemiter, $aUrl);

            if ($oOwner->{$this->urlAttribute} != $sUrl) {

                $oOwner->{$this->urlAttribute} = $sUrl;
                $attributeToSave[] = $this->urlAttribute;
            }
        }

        $sOldPath = $oOwner->{$this->pathAttribute};

        if ($oOwner->{$this->pathAttribute} != $sPath) {

            $oOwner->{$this->levelAttribute} = sizeof($aPath);
            $oOwner->{$this->pathAttribute} = $sPath;

            $attributeToSave[] = $this->pathAttribute;
            $attributeToSave[] = $this->levelAttribute;
        }

        if (sizeof($attributeToSave) > 0) {

            $_isNewRecord = $oOwner->isNewRecord;
            $oOwner->isNewRecord = false;
            $oOwner->saveAttributes($attributeToSave);
            $oOwner->isNewRecord = $_isNewRecord;

            if (!$oOwner->isNewRecord) {
                $this->_calculateLevelPathChildrens($sOldPath, $oOwner->{$this->pathAttribute}, $this->buildUrl ? $oOwner->{$this->urlAttribute} : null);
            }
        }

        if ($this->calculateChildrens)
            $this->updateChildrensList($sOldPath);
    }

    public function updateChildrensList($oldPath = null)
    {

        $oOwner = $this->getOwner();

        $aChildrens = $this->getChildrensId();
        $oOwner->{$this->childrensAttribute} = sizeof($aChildrens) > 0 ? join(',', $aChildrens) : null;

        $_isNewRecord = $oOwner->isNewRecord;
        $oOwner->isNewRecord = false;
        $oOwner->saveAttributes([$this->childrensAttribute]);
        $oOwner->isNewRecord = $_isNewRecord;

        if ((int)$oOwner->{$this->parentAttribute} > 0 && null !== ($oParents = $this->getParents())) {
            foreach ($oParents as $itm) {
                $itm->updateChildrensList();
            }
        }

        if (null !== $oldPath && $oldPath != $oOwner->{$this->pathAttribute}) {

            $path = explode('/', $oldPath, -1);
            foreach ($this->getOwner()->findAllByPk($path) as $itm) {
                $itm->updateChildrensList();
            }
        }
    }

    public function afterSave($event)
    {

        $this->calculateLevelPath();

        return parent::afterSave($event);
    }

    public function afterDelete($event)
    {

        parent::afterDelete($event);

        foreach ($this->getChildrens(1) as $itm) {
            $itm->delete();
        }

        if ($this->calculateChildrens)
            foreach ($this->getParents() as $itm) {
                $itm->updateChildrensList();
            }
    }

    protected function _calculateLevelPathChildrens($oldPath, $newPath, $url = null)
    {

        $oOwner = $this->getOwner();
        $pkCol = $oOwner->getMetaData()->tableSchema->primaryKey;

        $_oCriteria = new \CDbCriteria;

        $_oCriteria->addCondition('t.`path` LIKE :path');
        $_oCriteria->params[':path'] = $oldPath . '/%';

        $this->_applyMultyRootCriteria($_oCriteria);

        $aItems = [];
        foreach ($oOwner->findAll($_oCriteria) as $itm) {
            $aItems[(int)$itm->{$this->parentAttribute}][(int)$itm->{$pkCol}] = $itm;
        }

        if (isset($aItems[$oOwner->{$pkCol}]))
            $this->_calculateLevelPathRecursive($aItems, $oOwner->{$pkCol}, $newPath, $pkCol, $this->buildUrl ? $url : null);
    }

    protected function _calculateLevelPathRecursive(&$items, $pid, $parentPath, $pkCol, $parentUrl)
    {

        $sUrl = null;
        $level = sizeof(explode('/', $parentPath)) + 1;
        foreach ($items[$pid] as $itm) {

            $sPath = $parentPath . '/' . $itm->{$pkCol};

            $attributesToSave = [$this->levelAttribute => $level, $this->pathAttribute => $sPath];

            if ($this->buildUrl) {
                $sUrl = $parentUrl . $this->urlPartDelemiter . $itm->{$this->urlPartAttribute};
                $attributesToSave[$this->urlAttribute] = $sUrl;
            }

            $itm->saveAttributes($attributesToSave);

            if (isset($items[$itm->{$pkCol}])) {
                $this->_calculateLevelPathRecursive($items, $itm->{$pkCol}, $sPath, $pkCol, $sUrl);
            }
        }
    }

    public function getHierarchyList(Array $exclude = [])
    {

        $oOwner = $this->getOwner();
        $pkCol = $oOwner->getMetaData()->tableSchema->primaryKey;

        $_oCriteria = new \CDbCriteria;

        if ([] !== $exclude)
            $_oCriteria->addNotInCondition($pkCol, $exclude);

        //$_oCriteria->mergeWith($oOwner->getDbCriteria());
        //$this->_applyMultyRootCriteria($_oCriteria);

        $aItems = $this->prepareHierarchy($oOwner->findAll($_oCriteria));

        if (isset($aItems[0]))
            return self::_buildHierarchyList($aItems, $pkCol);

        return [];
    }

    public function getHierarchyTree($criteria = null)
    {
        return $this->prepareHierarchy($this->getOwner()->findAll($criteria));
    }

    public function prepareHierarchy(array $aData)
    {

        $aItems = [];
        foreach ($aData as $itm)
            $aItems[(int)$itm->{$this->parentAttribute}][] = $itm;

        return $aItems;
    }

    public static function _buildHierarchyList(Array $aData, $pkCol, $pid = 0)
    {

        $aResult = [];
        foreach ($aData[(int)$pid] as $itm) {

            $aResult[] = $itm;

            if (isset($aData[(int)$itm->{$pkCol}]))
                $aResult = \CMap::mergeArray($aResult, self::_buildHierarchyList($aData, $pkCol, $itm->{$pkCol}));
        }

        return $aResult;
    }

}
